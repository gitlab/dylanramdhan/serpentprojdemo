# EC327 Final Project - Group 2 - Serpent Game
---
## Summary
### Category
Animated Game

### TL;DR
Serpent Game is a snake like game where your goal is to survive the longest to get the highest score. 

### Description
Serpent Game is a snake-like 2D game where your goal is the survive as long as possible to secure the highest score. Your snake starts as a Unit Spot Square and as you eat apples, you get bigger and your score gets incremented by 50. Your score also gets incremented by 5 for every second spent in the game. In addition, every 250 points, a barrier spawns randomly on the board, increasing the difficulty to stay alive. Interaction with the snake happens through either a D-Pad on the bottom of the screen or through the screen itself if in TouchScreen mode (see settings). The game allows you to modify 3 things: the color of the snake (Red, Yellow, or Blue), the mode of interaction with the snake (D-Pad or TouchScreen), and the speed of the game (slow, medium, or fast).

## Authors
### Group members
1. EmilyMcCarthy - 
2. LouisJimenezHernandez - U45892561
3. LeonardoMattosMartins - U25267206
4. SergioRodriguezRivera - U97160016
5. NeekonNejad - U76703007
6. ZaidBhatti - U92489792
7. KoenLin - U99385844
8. OdilonQuevillon - U94630332

### Roles
1. Projects Leads: Sergio Rodriguez Rivera[50%], OdilonQuevillon[50%]
2. Back End: LeonardoMattosMartins[33.3%], NeekonNejad[33.3%], ZaidBhatti[33.3%]
3. Front End: LouisJimenzHernandez[33.3%], LeonardoMattosMartins[33.3%], SergioRodriguezRivera[33.3%]
4. Documenter: EmilyMcCarthy[100%]
5. Tester: KoenLin[100%]

---
## Accomplishments

### Minimum requirements

Minimum Requirements satisfied:
1. Moving graphical pieces -> Satisfied through the snake moving accross the board.
2. The ability of the user to control some of the moving pieces through input -> Satisfied through the two input mode (D-Pad and TouchScreen).
3. Include a score that is changed based on the user satisfying requirements -> Satisfied through the score being incremented by 50 points when the user's snake eats an apple.
4. Have a simple-to-use, graphical user interface. -> Satisfied with the 4 main screens (Main Menu, Rules, Settings, Game Screen).
5. Provide clear documentation of the rules of the game. -> Satisfied with the Rules screen.
6. Not require any Internet or network connectivity. -> Satisfied by being able to run the entire app without Interner or network connectivity.
7. Not violate the copyright of Tetris or any other game. -> Altough our game is fairly similar to the traditional Snake Game, ours has its own specifications that makes it different. Such as the goal of the game, the score policy, and the barriers. Therefore not violating the coryright of Snake Game.

### Possible features

Possible Features Satisfied:
1. Allow the user to interact with the game by moving the phone.[10%] -> Satisfied by the ability of the user to change the color of the snake by shaking the phone.
2. Provide a high-score list that persists when the app is closed and then reopened.[10%] -> Satisfied by having a list of the 10 highest previous scores scored by the user since downloading the app.
3. Allow the user to tweak the rules of the game being played.[10%] -> Satisfied by the ability for the user to change the speed of the snake, and the frequency at which the barriers spawn.
4. Add sound effects for specific activities in the game.[10%] -> Satisfied by the sounds made when the snake eats an apple.

Possible Features Partially Satisfied:
1. Allow the user to adapt all significant aspects of the Graphical User Interface.[10%] -> Partially satisfied (probably 6% out of 10) through the ability for the user to choose the color of the snake as well as the mode of interaction with the snake (D-Pad or TouchScreen). Not fully satisfied because not all significant aspects of the GUI can be modified by the user. Could have made the color of the board or the shape of the buttons modifiable.
2. Do all calculations in C++ and connect them to the app through Android's Native Development Kit.[20%] -> Partially satisfied (probably 12% out of 20) through the snake's movements, the obstacles spawning, and the score calculations made in C++ then connected to Android's NDK. Only partially satisfied because not all calculations were made in C++, a lot of them were made directly in Java such as checking when the game is over or checking when the snake hits a wall.

---
## Execution

### Project source
https://agile.bu.edu/gitlab/ec327_projects/group2project.git

### Installation
To run this game, you need Android App Studio.
Once you have it, follow the following instructions:
1. Download the Zip file found here and open the file with Android Studio
2. Create a new Android Studio project.  
3. Set up the emulator, we recommend API 30. Note that the lower the API, the easier it will be for your computer to run the app. Note as well that the game is intended to run on Pixel 4. Running on different versions might create graphical imperfections.
4. Once all files are loaded and the emulator is set up, run the app!
Note: if you get an error saying "SDK location not found", go to File -> Invalidate Caches... then select all three checkboxes and hit "Invalidate and Restart"

### Usage
When running the app, 4 different screens are available for the user to jump to. The first screen is the main menu which contains a "Play" button, a "Settings" button, and a "Rules" button. When clicking the rules button, the user is redirected to the rules screen which includes the basic rules of the game. To comeback to the Main Menu, click the "Return to Main Menu" button. When clicking the "Settings" button, the user is redirected to the Settings screen which includes the different settings the user is allowed to modify. Such as the snake's color and speed, and the mode of interaction with the snake (D-Pad or TouchScreen). To comeback to the main menu, click the "Return to Main Menu" button. To play the game, click the "Start" button. When clicking the "Start" button, the user is redirected to the game screen where the game takes place. Depending on which mode of input the user selected, the user can control the snake through either the D-Pad buttons or the screen itself if in TouchScreen mode.

---
## Miscellaneous

### Extra features
None.

### Challenges
During the implementation of this project, the one big challenge that arose was the link between C++ and Java through the JNI. We first wrote lots of classes in C++ for the Back-End and had to rewrite a lot of it because of the challenge of the JNI.

### Supporting material
https://docs.google.com/presentation/d/1lkNfZ5l8K-hfs1A8tmzy0tCaaJF_1Dv3rKy8oSuuCxw/edit?usp=sharing

### Release
We are all comfortable with having this project visible to the broader public.

###
